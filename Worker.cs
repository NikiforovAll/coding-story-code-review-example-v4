using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace StatusWorker
{
    public class Worker : BackgroundService
    {
        private readonly TimeSpan _delay = TimeSpan.FromSeconds(5);
        private readonly string _serviceUrl;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<Worker> logger;

        public Worker(IHttpClientFactory httpClientFactory, IConfiguration configuration, ILogger<Worker> logger)
        {
            _serviceUrl = configuration["StatusServiceBaseUrl"]
                ?? throw new ArgumentException("Unable to configure client");
            _httpClientFactory = httpClientFactory;
            this.logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var httpClient = _httpClientFactory.CreateClient();
                var response = await httpClient.GetAsync(_serviceUrl, stoppingToken);
                logger.LogInformation($"Received: {response.StatusCode}");
                await Task.Delay(_delay, stoppingToken);
            }
        }
    }
}
